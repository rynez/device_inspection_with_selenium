#!/usr/bin/env python
# -*- encoding:utf-8 -*-
# @ModuleName: inspectiontool
# @Author: RyneZ
# @Time: 2022-08-04 5:00

import os
import time
import logging
import sys
import selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from pathlib import Path
from PyQt5.QtCore import pyqtSignal, QThread,QObject
from selenium.common.exceptions import *
from PyQt5.QtGui import QPixmap
from PyQt5.Qt import QSize, QImageReader
from PyQt5.QtWidgets import *
from docx import Document
from docx.shared import Cm
from mainDisplay_UI import *
import win32api,win32con

chrome_options = Options()
__browser_url = r'.\Google Chrome\Chrome-bin\chrome.exe'
chromedriver_path = r'.\Google Chrome\Chrome-bin\chromedriver.exe'
options=Options()
ISSTOP=0
######自动登录
#browser=webdriver.Chrome()
#自定义chrome和驱动地址
options.binary_location = __browser_url
#options.add_argument("--user-data-dir=.\Google Chrome\data")
options.add_argument("-ignore-certificate-errors")
options.add_argument("-disable-web-security")
options.add_argument("-disable-site-isolation-trials")
options.add_experimental_option('excludeSwitches', ['enable-automation', 'enable-logging'])
options.add_experimental_option('useAutomationExtension', False)
options.add_argument("-disable-xss-auditor")
options.add_argument('-disable-gpu')
options.add_argument('-headless')
VK_CODE = {"ctrl":17,"v":86,"enter":13}# 键盘映射字典

def keyDown(keyName):
    win32api.keybd_event(VK_CODE[keyName], 0, 0, 0)
    # 按下按键

def keyUp(keyName):
    win32api.keybd_event(VK_CODE[keyName], 0, win32con.KEYEVENTF_KEYUP, 0)
    # 松开按键

class prtscthread(QObject):
    isstop=pyqtSignal(str)
    noticestr=pyqtSignal(str)
    getnewvalicode=pyqtSignal()
    picfilename=pyqtSignal(str)
    def __init__(self,usr,pwd,vlicode,browser):
        super(prtscthread, self).__init__()
        self.usr=usr
        self.pwd=pwd
        self.vlicode=vlicode
        self.browser=browser
        self.foldername=''
    def run(self):
        self.noticestr.emit('开始巡检')
        self.browser.find_element_by_xpath("//input[@id='username'][1]").clear()
        self.browser.find_element_by_xpath("//input[@id='passwd'][1]").clear()
        self.browser.find_element_by_xpath("//input[@id='username'][1]").send_keys(self.usr)  # 输入用户名
        self.browser.find_element_by_xpath("//input[@id='passwd'][1]").send_keys(self.pwd)  # 输入密码
        if self.browser.find_element_by_xpath("//input[@id='Txtidcode']"):
            self.browser.find_element_by_xpath("//input[@id='Txtidcode'][1]").clear()
            self.browser.find_element_by_xpath("//input[@id='Txtidcode'][1]").send_keys(self.vlicode)  # 输入验证码
        self.browser.find_element_by_xpath("//img[@id='Image1']").click()  # 点击登录
        time.sleep(1)
        #检测弹窗
        try:
            alter = self.browser.switch_to.alert
            #print(alter.text)
            self.noticestr.emit('页面提示如下：'+alter.text)
            alter.accept()
            self.getnewvalicode.emit()
            return 0
        except NoAlertPresentException:
            pass
        WebDriverWait(self.browser, 30, 0.2).until(lambda browser: browser.find_element_by_xpath("/html/frameset"))
        ######进入frame


        self.browser.switch_to.frame('Content')  # Content
        self.browser.switch_to.frame("leftFrame")  # 进入leftFrame
        #2.2.2.2CPU利用率
        #2.2.2.3内存利用率
        #2.2.2.4磁盘空间利用率
        #time.sleep(2)
        title_list= ['2.2.2.2CPU利用率','2.2.2.3内存利用率','2.2.2.4磁盘空间利用率']
        for i in title_list:
            self.screenshot(i)

        try:
            #2.2.2.5接口状态
            self.browser.find_element_by_xpath("//img[@id='image_network']").click() #网络管理
            self.browser.find_element_by_xpath("//font[@id='mTitle1616']").click()#接口
            time.sleep(2)
            self.checkStatus()
            self.screenshot('2.2.2.5接口状态')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')



        try:
            #2.3.2.1开放服务配置
            self.browser.find_element_by_xpath("//img[@id='image_system']").click() #系统管理
            self.browser.find_element_by_xpath("//font[@id='mTitle1609']").click()#配置
            time.sleep(1)

            self.browser.switch_to.default_content()
            self.browser.switch_to.frame('Content')  #Content
            time.sleep(1)
            self.browser.switch_to.frame('mainFrame')#进入frame#进入mainframe
            self.browser.find_element_by_xpath("//*[@id='ArticleBody']/tbody/tr/td/a[2]").click() #开放服务
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.1开放服务配置')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.2时间同步配置
            self.browser.find_element_by_xpath("//*[@id='ArticleBody']/tbody/tr/td/a[3]").click() #时间
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.2时间同步配置')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.3接口状态
            self.browser.switch_to.default_content()
            self.browser.switch_to.frame('Content')  #Content
            self.browser.switch_to.frame("leftFrame")#进入leftframe
            self.browser.find_element_by_xpath("//img[@id='image_network']").click() #网络管理
            self.browser.find_element_by_xpath("//font[@id='mTitle1616']").click()#接口
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.3接口状态')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.4路由配置
            self.browser.find_element_by_xpath("//font[@id='mTitle1618']").click()#路由
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.4路由配置')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.5用户配置
            self.browser.find_element_by_xpath("//img[@id='image_system']").click() #系统管理
            self.browser.find_element_by_xpath("//font[@id='mTitle1612']").click()#管理员
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.5用户配置')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
        #2.3.2.6 入侵防御策略配置
            self.browser.find_element_by_xpath("//img[@id='image_ips']").click() #安全防护
            self.browser.find_element_by_xpath("//font[@id='mTitle1632']").click()#入侵防御策略
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.6 入侵防御策略配置')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.7 统计信息状态
            self.browser.find_element_by_xpath("//img[@id='image_monitor']").click()#监控信息
            self.browser.find_element_by_xpath("//font[@id='mTitle1603']").click()#攻击统计
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.7 统计信息状态')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')

        try:
            #2.3.2.8 日志状态
            self.browser.find_element_by_xpath("//img[@id='image_log']").click()#日志报表
            self.browser.find_element_by_xpath("//font[@id='mTitle1646']").click()#安全日志
            time.sleep(2)
            if ISSTOP:
                return 0
            self.screenshot('2.3.2.8 日志状态')
        except NoSuchElementException:
            self.noticestr.emit('<span style="color:yellow">未找到对应项，已跳过</sapn>')


        #self.screenshot('test')
        self.noticestr.emit('巡检结束')
        self.isstop.emit(self.foldername)

    def screenshot(self,pname):
        pfilename = u'.\\Shot'

        current_time = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        if not self.foldername:
            self.foldername=pfilename + current_time
        pic_path = self.foldername + '\\'  + pname + '.png'

        if Path(self.foldername).is_dir():
            pass
        else:
            Path(self.foldername).mkdir()
        self.browser.save_screenshot(pic_path)
        self.picfilename.emit(pic_path)

    def checkStatus(self):
        if ISSTOP:
            print(ISSTOP)
            sys.exit()


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        # 初始化UI
        self.setupUi(self)
        # self.browser = webdriver.Chrome(chromedriver_path, options=options)  # 打开浏览器,生成实例
        # self.browser.set_window_size(1400, 1000)
        self.getcodeButton.clicked.connect(self.getcodeButtonClicked)
        self.submitvalicodeButton.clicked.connect(self.submitvalicodeButtonClicked)
        self.stopButton.clicked.connect(self.stopButtonClicked)
        self.submitvalicodeButton.setEnabled(False)
        self.stopButton.setEnabled(False)


    def getcodeButtonClicked(self):
        try:

            self.browser = webdriver.Chrome(chromedriver_path, options=options)  # 打开浏览器,生成实例
            self.browser.set_window_size(1400, 1000)
        except Exception:
            self.loginfo.append('<span style="color:red">未能正常启动chrome进程，请检查目录下文件是否完整。</span>')
        self.ipaddr=self.ipaddrEdit.text().strip()
        self.port=self.portEdit.text().strip()
        self.browser.set_page_load_timeout(5)
        try:
            self.browser.get('https://'+self.ipaddr+':'+self.port+'/')  # 打开页面
            WebDriverWait(self.browser, 8, 0.2).until(lambda browser : browser.find_element_by_xpath('//*[@id="ehong-code"]/font'))
            valicodes = self.browser.find_elements_by_xpath('//*[@id="ehong-code"]/font')
        except TimeoutException:
            self.loginfo.append('<span style="color:red">访问页面超时，请检查IP地址或者端口是否正确！</span>')
            return 0

        if len(valicodes)==4:
            valistr=''
            for valicode in valicodes:
                valistr=valistr+valicode.text
            self.valicode.setText(valistr)
            self.submitvalicodeButton.setEnabled(True)
            self.loginfo.append('<span style="color:blue">检查到验证码：'+valistr+' ，可以点击开始巡检按钮进行后续过程</span>')
        elif len(valicodes)==0:
            self.submitvalicodeButton.setEnabled(True)
            self.loginfo.append('无验证码，可以点击开始巡检按钮进行后续过程')
        else :
            self.loginfo.append('<span style="color:red">验证码识别错误，无法进行巡检</sapn>')
            return 0

    def submitvalicodeButtonClicked(self):
        global ISSTOP
        username=str(self.userEdit.text())
        password=str(self.pwdEdit.text())
        valicode=str(self.valicode.text())

        self.checkthreadact=prtscthread(username,password,valicode,self.browser)
        self.mycheckthread = QThread()
        self.checkthreadact.moveToThread(self.mycheckthread)
        self.mycheckthread.started.connect(self.checkthreadact.run)
        #self.checkthreadact.finished.connect(self.checkfinished)
        self.mycheckthread.start()
        ISSTOP=0
        self.stopButton.setEnabled(True)

        self.checkthreadact.isstop.connect(self.pictodocx)
        self.checkthreadact.noticestr.connect(self.loginfoupdate)
        self.checkthreadact.picfilename.connect(self.picShow)
        self.checkthreadact.getnewvalicode.connect(self.getcodeButtonClicked)
        self.mycheckthread.quit()

    def stopButtonClicked(self):
        self.mycheckthread.quit()
        global ISSTOP
        ISSTOP = 1
        self.stopButton.setEnabled(False)
        self.submitvalicodeButton.setEnabled(False)
        self.loginfo.append('<span style="color:red">已手动停止，请重新获取验证码进行登录</span>')

    #根据截图生成文档
    def pictodocx(self,folderstr):
        self.loginfo.append('开始生成巡检报告...')
        docxfile = '天融信入侵防御系统巡检模板.docx'

        files = (os.listdir(folderstr))
        document = Document(docxfile)
        for table in document.tables:
            try:
                picseq = table.rows[0].cells[1].text.strip()
                for file in files:
                    if file.startswith(picseq):
                        #print(file, table.rows[0].cells[3].text)
                        run = table.rows[-1].cells[3].paragraphs[0].add_run('')
                        run.add_break()
                        run.add_picture(folderstr +'/'+ file, width=Cm(14))
            except IndexError:
                self.loginfo.append('<span style="color:red">处理'+picseq+'时表格出现问题，已跳过</span>')
                continue
        outfilename=self.ipaddr+'巡检报告.docx'
        document.save(outfilename)
        self.loginfo.append('已完成输出路径为：'+outfilename)

        #顺便退出
        self.browser.switch_to.default_content()
        self.browser.switch_to.frame('topFrame')  # topFrame

        self.browser.find_element_by_xpath("//img[@id='Image1']").click()  # 退出登录
        time.sleep(1)
        alter = self.browser.switch_to.alert
        alter.accept()
        self.browser.close()
        self.submitvalicodeButton.setEnabled(False)
        self.stopButton.setEnabled(False)

    def loginfoupdate(self,str):
        self.loginfo.append(str)

    def picShow(self,str):
        img = QImageReader(str)
        scale = 768 / img.size().width()
        height = int(img.size().height() * scale)
        img.setScaledSize(QSize(768, height))
        img = img.read()
        pixmap = QPixmap(img)
        self.imgLabel.setPixmap(pixmap)

        self.loginfo.append('已成功截图，文件路径为:'+str)



if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO, filename='log', format="%(asctime)s %(levelname)s：%(funcName)s：%(message)s")
    # 适应高DPI设备
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    # 适应Windows缩放
    QtGui.QGuiApplication.setAttribute(QtCore.Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    app = QApplication(sys.argv)
    mainWindow = MainWindow()
    mainWindow.show()
    sys.exit(app.exec())